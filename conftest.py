import pytest


def pytest_addoption(parser):
    parser.addoption("--stringToSearch", action="store")

@pytest.fixture(scope='session')

def stringToSearch(request):
    stringToSearch_value = request.config.option.stringToSearch
    return stringToSearch_value
    
    
