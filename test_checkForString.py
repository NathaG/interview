import urllib

def checkForString(url, stringToSearch):
    page = urllib.urlopen(url).read()
    
    if page.find(stringToSearch) == -1:
        return False
    else:
        return True
        
def test_checkForString(stringToSearch):
    url = 'https://the-internet.herokuapp.com/context_menu'
    assert True == checkForString(url, stringToSearch)
    
    
			
			